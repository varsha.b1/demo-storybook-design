import { NgModule } from '@angular/core';
import { PatternStorybookComponent } from './pattern-storybook.component';
import { ButtonComponent } from './button/button.component';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [PatternStorybookComponent, ButtonComponent],
  imports: [CommonModule
  ],
  exports: [PatternStorybookComponent, ButtonComponent]
})
export class PatternStorybookModule { }
