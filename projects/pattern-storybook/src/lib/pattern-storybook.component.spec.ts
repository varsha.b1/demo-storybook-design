import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatternStorybookComponent } from './pattern-storybook.component';

describe('PatternStorybookComponent', () => {
  let component: PatternStorybookComponent;
  let fixture: ComponentFixture<PatternStorybookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatternStorybookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatternStorybookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
