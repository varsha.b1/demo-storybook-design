import { TestBed } from '@angular/core/testing';

import { PatternStorybookService } from './pattern-storybook.service';

describe('PatternStorybookService', () => {
  let service: PatternStorybookService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatternStorybookService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
