import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pl-pattern-storybook',
  template: `
    <p>
      pattern-storybook works!
    </p>
  `,
  styles: [
  ]
})
export class PatternStorybookComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
